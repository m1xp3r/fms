<?php
include 'inc/header.php';

if (!$_SESSION['authenticated']) {
    header('Location: login');
    exit;
}
?>
    <div class="container">
        <h1 class="mb-4 text-center">Power Family Management System</h1>
        <div class="text-center">
            <h2>Expenses</h2>
            <ul class="nav nav-pills nav-fill">
                <li class="nav-item">
                    <button name="new" id="new" onclick="window.location.href='newExpense'" class="nav-link"><i class="fa-solid fa-plus"></i> New
                        Expense
                    </button>
                </li>
                <li class="nav-item">
                    <button name="list" id="list" onclick="window.location.href='listExpenses'" class="nav-link"><i class="fa-solid fa-list"></i> List
                        Expenses
                    </button>
                </li>
                </button></ul>
        </div>
    </div>
<?php
include 'inc/footer.php';
