const familyMemberSelect = document.getElementById("familyMember");
const categorySelect = document.getElementById("category");
const categoryLabel = document.getElementsByName("category")[0].parentNode;
const benefitsInput = document.getElementById("benefits");
const benefitsElement = document.getElementById("benefits").parentNode;
const form = document.querySelector("form");
const receiptValueInput = document.getElementById("receipt_value");
let isTyping = false;

function handleFamilyMemberChange() {
    categorySelect.selectedIndex = 0;
    const selectedFamilyMember = familyMemberSelect.value;
    if (selectedFamilyMember === "") {
        // Disable all form fields when family member is not selected
        disableFormFields(true);
        categorySelect.disabled = true;
        categorySelect.hidden = true;
        categorySelect.selectedIndex = 0;
        categoryLabel.hidden = true;
    } else {
        // Enable category select
        categorySelect.disabled = false;
        categorySelect.hidden = false;
        categoryLabel.hidden = false;
    }
    handleCategoryChange();
}

function handleCategoryChange() {
    const selectedCategory = categorySelect.value;
    const disableFields = (selectedCategory === "");
    // Disable or enable form fields based on category selection
    disableFormFields(disableFields);
    benefitsInput.disabled = (categorySelect.value !== "medical");
    benefitsElement.style.display = (categorySelect.value !== "medical") ? "none" : "block";
}

function disableCategories() {

    let i;
    if (familyMemberSelect.value === "2") { // Julia selected
        const disabledOptions = ["extracurricular", "education", "clothing", "entertainment"];

        for (i = 1; i < categorySelect.options.length; i++) {
            const option = categorySelect.options[i];
            option.disabled = disabledOptions.includes(option.value);
        }

        handleCategoryChange();
    } else {
        for (i = 1; i < categorySelect.options.length; i++) {
            categorySelect.options[i].disabled = false;
        }
    }
}

function disableFormFields(disable) {
    const inputs = form.querySelectorAll("div[class~='disabled'], input[type='text'], textarea, input[type='date'], input[type='number'], input[type='file']");
    inputs.forEach(function (input) {
        input.disabled = disable;
        input.hidden = disable;

        if (disable) {
            input.value = "";
            receiptValueInput.value = formatCurrency(0.00);
        }
    });
}

function formatCurrency(amount) {
    const numericAmount = parseFloat(amount);
    return numericAmount.toLocaleString("en-US", {
        style: "currency",
        currency: "USD"
    });
}

function formatReceiptValueCurrency(amount) {
    receiptValueInput.value = amount.replace(/[^\d.]/g, "");
    receiptValueInput.select();
    isTyping = true;
}

// Reset form to initial state
function resetForm() {
    form.reset();
    handleFamilyMemberChange()
    handleCategoryChange();
    familyMemberSelect.selectedIndex = 0;
    categorySelect.disabled = true;
    receiptValueInput.value = formatCurrency(0.00);
}

resetForm();

// Event listeners
familyMemberSelect.addEventListener("change", handleFamilyMemberChange);
document.getElementById("familyMember").addEventListener("change", disableCategories);
categorySelect.addEventListener("change", handleCategoryChange);
receiptValueInput.addEventListener("input", function () {
    isTyping = true;
});
receiptValueInput.addEventListener("blur", function () {
    if (isTyping) {
        const amount = this.value.replace(/[^\d.]/g, "");
        if (amount !== "") {
            this.value = formatCurrency(amount);
        }
        isTyping = false;
    }
});
