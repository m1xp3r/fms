<?php
include 'inc/header.php';
include 'usr/config.inc.php';

/**
 * @var string $apiKey
 * @var string $spaceId
 * @var string $listId
 */

if (!$_SESSION['authenticated']) {
    header('Location: login');
    exit;
}
?>
    <div class="container">
        <h1 class="mb-4">Expense Reimbursement List</h1>
        <div class="row">
            <div class="col">
                <table class="table table-responsive-md">
                    <thead>
                    <tr>
                        <th>Expense</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Amount</th>
                        <th>Reimbursed</th>
                        <th>Updated</th>
                        <th>Admin</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    function getTasks($apiKey, $listId)
                    {
                        $url = "https://api.clickup.com/api/v2/list/{$listId}/task?include_closed=true";
                        $headers = array(
                            "Authorization: {$apiKey}",
                            "Content-Type: application/json"
                        );

                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_URL, $url);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                        $response = curl_exec($curl);
                        curl_close($curl);

                        return json_decode($response, true);
                    }

                    $tasks = getTasks($apiKey, $listId);

                    foreach ($tasks['tasks'] as $task) {

                        foreach ($task['custom_fields'] as $customField) {
                            if ($customField['id'] === '340f9614-7303-44bd-8fdb-7ad7ed6b48ab') {
                                $amountReimbursed = $customField['value'];
                            }
                            if ($customField['id'] === '0bc6bf00-b4e9-491e-85cc-868826efa49c') {
                                $amountRequested = $customField['value'];
                            }
                        }

                        $status = $task['status']['status'];

                        echo '<tr class="bg-light" id="' . $task['id'] . '">';
                        echo '<td><a onclick="copyIdToClipboard(\'' . $task['id'] . '\')" href="#' . $task['id'] . '">' . $task['name'] . '</a></td>';
                        echo '<td style="color: ' . $task['tags'][0]['tag_bg'] . '">' . ucwords($task['tags'][0]['name']) . '</td>';
                        echo '<td style="color: ' . $task['status']['color'] . '">' . ucwords($status) . '</td>';
                        echo '<td>$' . $amountRequested . '</td>';
                        echo '<td>$' . $amountReimbursed . '</td>';
                        echo '<td>' . date("d M y", substr($task['date_updated'], 0, -3)) . '</td>';
                        echo '<td><a target="_blank" href="' . $task['url'] . '"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
  <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
</svg></a></td>';
                        echo '</tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="text-center">
                <button type="button" onclick="window.location.href='index'" class="btn btn-primary"><i class="fa-solid fa-house"></i> Home</button>
            </div>
        </div>
    </div>
<script>
    function copyIdToClipboard(id) {
        const tempInput = document.createElement('input');
        tempInput.value = id;
        document.body.appendChild(tempInput);
        tempInput.select();

        // Use the Clipboard API to copy the selected value
        navigator.clipboard.writeText(tempInput.value)
            .then(() => {
                console.log('Text copied to clipboard');
            })
            .catch((error) => {
                console.error('Failed to copy text to clipboard:', error);
            });

        document.body.removeChild(tempInput);
    }
</script>
<?php
include 'inc/footer.php';
