<?php
include 'inc/header.php';
include 'usr/config.inc.php';
/**
 * @var string $validPassword
 */
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $password = $_POST['password'] ?? '';

    if ($password === $validPassword) {
        // Authentication successful
        $_SESSION['authenticated'] = true;
        header('Location: index');
        exit;
    } else {
        // Authentication failed
        $errorMessage = 'Invalid password.';
    }
}
?>
<div class="container">
    <h1 class="my-4">Login</h1>
    <?php if (isset($errorMessage)): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorMessage; ?>
        </div>
    <?php endif; ?>
    <form method="POST">
        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" class="form-control" id="password" name="password" required>
        </div>
        <button type="submit" class="btn btn-primary">Login</button>
    </form>
</div>
