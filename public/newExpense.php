<?php include 'inc/header.php';
if (!$_SESSION['authenticated']) {
    header('Location: login');
    exit;
}
?>

<div class="container">
    <h1 class="mb-4 text-center">New Expense Reimbursement Request</h1>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <form action="submitExpense" method="POST" enctype="multipart/form-data">
                <div class="mb-3">
                    👩‍👧‍👧 <strong><label for="familyMember" class="form-label">Family Member</label></strong> <i
                            title="Required" class="fa-solid fa-star"></i>️
                    <select name="family_member" id="familyMember" class="form-select" required>
                        <option class="dropdown-item" value="" selected="selected" disabled="disabled">Choose Name
                        </option>
                        <option value="0">👧🏻 Abby</option>
                        <option value="1">👧🏻 April</option>
                        <option value="2">👩🏻 Julie</option>
                    </select>
                </div>
                <div class="mb-3">
                    🧐 <strong><label for="category" class="form-label">Expense Category</label></strong> <i
                            title="Required" class="fa-solid fa-star"></i>️️
                    <select name="category" id="category" class="form-select" required>
                        <option value="" selected="selected" disabled="disabled">Choose Category</option>
                        <option id="medical" value="medical">🏥 Medical Expenses (Benefits Claim)</option>
                        <option id="extracurricular" value="extracurricular">🏆 Extracurricular Activities</option>
                        <option id="education" value="education">🎓 School Fees/Supplies</option>
                        <!--<option id="clothing" value="clothing">👚 Clothing</option>-->
                        <!--<option id="entertainment" value="entertainment">🎪 Entertainment</option>-->
                        <option id="other" value="other">😳 Other</option>
                    </select>
                </div>
                <div class="mb-3 disabled">
                    🛍️ <strong><label for="name" class="form-label">Expense Item</label></strong> <i title="Required"
                                                                                                     class="fa-solid fa-star"></i>️
                    <input type="text" name="name" id="name" class="form-control" required
                           placeholder="What is this expense for? Ie. Eye Exam, Glasses, Dental, Field Trip, Etc.">
                </div>
                <div class="mb-3 disabled">
                    😅 <strong><label for="description" class="form-label">Additional Details and/or
                            Justification</label></strong>
                    <textarea name="description" id="description" class="form-control"
                              placeholder="Provide any additional details about this expense and a justification for reimbursement, if needed"></textarea>
                </div>
                <div class="mb-3 disabled">
                    📆 <strong><label for="date" class="form-label">Expense Date</label></strong> <i title="Required"
                                                                                                    class="fa-solid fa-star"></i>️️
                    <input type="date" name="date" id="date" class="form-control" required>
                </div>
                <div class="mb-3 disabled">
                    💰 <strong><label for="receipt_value" class="form-label">Total Expense Amount</label></strong> <i
                            title="Required" class="fa-solid fa-star"></i>️
                    <input type="text" onclick="formatReceiptValueCurrency(this.value);" id="receipt_value"
                           name="receipt_value" placeholder="$0.00" class="form-control" required>
                </div>
                <div class="mb-3 disabled">
                    🧾 <strong><label for="receipt" class="form-label">Receipt</label></strong> <i title="Required"
                                                                                                  class="fa-solid fa-star"></i>️
                    <input type="file" name="receipt" id="receipt" class="form-control-file" required>
                </div>
                <div class="mb-3 disabled">
                    🧾 <strong><label for="benefits" class="form-label"> Benefits Receipt</strong></label> <i
                            title="Required" class="fa-solid fa-star"></i>️
                    <input type="file" name="benefits" id="benefits" class="form-control-file" required>
                </div>
                <div class="text-center">
                    <button title="submit" type="submit" class="btn btn-lg btn-success"><i
                                class="fa-solid fa-paper-plane"></i> <strong>Submit</strong></button>
                </div>
                <div class="text-center mt-3">
                    <button type="button" onclick="resetForm()" class="btn btn-warning"><i
                                class="fa-solid fa-arrows-rotate"></i>️ Reset
                    </button>
                    <button type="button" onclick="window.location.href='index'" class="btn btn-danger"><i
                                class="fa-solid fa-ban"></i> Cancel
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="js/newExpense.js"></script>
<?php include 'inc/footer.php'; ?>
