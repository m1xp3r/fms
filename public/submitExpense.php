<?php

include 'inc/header.php';
include 'usr/config.inc.php';

/**
 * @var string $apiKey
 * @var string $spaceId
 * @var string $listId
 */

if (!$_SESSION['authenticated']) {
    header('Location: login');
    exit;
}
?>

    <div class="container">
    <h1 class="mb-4 text-center">Power Family Expense Reimbursement System</h1>
    <div class="text-center">
<?php

// ClickUp API endpoint (task creation)
$taskEndpoint = "https://api.clickup.com/api/v2/list/{$listId}/task";

// Function to handle file upload
function handleFileUpload($file)
{
    $targetDirectory = 'uploads/';
    $targetFile = $targetDirectory . basename($file['name']);

    if (move_uploaded_file($file['tmp_name'], $targetFile)) {
        return $targetFile;
    } else {
        return false;
    }
}

// Function to remove the uploaded file
function removeFile($filePath)
{
    if (file_exists($filePath)) {
        unlink($filePath);
    }
}

// Check if form is submitted
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Get form data
    $familyMember = filter_var($_POST['family_member'], FILTER_SANITIZE_STRING);
    $taskName = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
    $taskDescription = filter_var($_POST['description'], FILTER_SANITIZE_STRING);
    $receiptValue = filter_var($_POST['receipt_value'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $receiptDate = strtotime(filter_var($_POST['date'], FILTER_SANITIZE_STRING)) . '000';
    $receiptCategory = filter_var($_POST['category'], FILTER_SANITIZE_STRING);;
    $receiptFile = $_FILES['receipt'];
    $benefitsFile = $_FILES['benefits'];

    // Handle receipt file upload
    $receiptFilePath = handleFileUpload($receiptFile);
    if (!$receiptFilePath) {
        echo '<div class="alert alert-danger">Failed to upload receipt. Please try again.</div>';
        exit;
    }

    //check that provider file is actually uploaded and not an empty form field
    if ($benefitsFile['size'] > 0) {
        $benefitsFileUploaded = true;
        $benefitsFilePath = handleFileUpload($benefitsFile);
        if (!$benefitsFilePath) {
            echo '<div class="alert alert-danger">Failed to upload provider receipt. Please try again.</div>';
            exit;
        }
    } else {
        $benefitsFilePath = '';
    }

    // Task details
    $taskPayload = [
        'name' => $taskName,
        'description' => $taskDescription,
        'status' => 'new',
        'assignees' => [
            32308626
        ],
        'tags' => [
            $receiptCategory
        ],
        'priority' => 3,
        'due_date' => strtotime('+2 week') . '000',
        'notify_all' => true,
        'custom_fields' => [
            [
                'id' => '0bc6bf00-b4e9-491e-85cc-868826efa49c',
                'value' => $receiptValue
            ],
            [
                'id' => 'a98d8588-056c-4937-80ed-84529efe9eaf',
                'value' => $receiptDate
            ],
            [
                'id' => '8617950f-c52e-403e-a7d6-bdb11f451f33',
                'value' => $familyMember
            ]
        ]
    ];

    // Convert task payload to JSON
    $jsonTaskPayload = json_encode($taskPayload);

    // Set up cURL for task creation
    $ch = curl_init($taskEndpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Content-Type: application/json',
        'Authorization: ' . $apiKey
    ]);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonTaskPayload);

    // Execute the task creation cURL request
    $response = curl_exec($ch);
    $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    if ($statusCode === 200) {
        $responseData = json_decode($response, true);
        $taskId = $responseData['id'];

        // Set up cURL for attachment upload
        $attachmentEndpoint = "https://api.clickup.com/api/v2/task/{$taskId}/attachment";
        $ch = curl_init($attachmentEndpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $apiKey
        ]);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'attachment' => new CURLFILE($receiptFilePath),
        ]);

        // Execute the attachment upload cURL request
        $attachmentResponse = curl_exec($ch);
        $attachmentStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($attachmentStatusCode === 200) {
            //upload benifits receipt if it exists
            if ($benefitsFileUploaded) {
                // Set up cURL for attachment upload
                $attachmentEndpoint = "https://api.clickup.com/api/v2/task/{$taskId}/attachment";
                $ch = curl_init($attachmentEndpoint);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Authorization: ' . $apiKey
                ]);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, [
                    'attachment' => new CURLFILE($benefitsFilePath),
                ]);

                // Execute the attachment upload cURL request
                $attachmentResponse = curl_exec($ch);
                $attachmentStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                curl_close($ch);
                if ($attachmentStatusCode === 200) {
                    removeFile($benefitsFilePath);
                } else {
                    echo '<div class="alert alert-danger">Failed to upload provider receipt. Please try again.<div><code>' . $response . '</code></div></div>';
                }
            }
            // Remove the uploaded receipt file
            removeFile($receiptFilePath);
            echo '<div class="alert alert-success">Expense created successfully. Receipt(s) uploaded.</div>';
        } else {
            echo '<div class="alert alert-danger">Failed to upload receipt. Please try again.<div><code>' . $response . '</code></div></div>';
        }
    } else {
        echo '<div class="alert alert-danger">Error creating Expense. Please try again.<div><code>' . $response . '</code></div></div>';
    }
} else {
    echo '<div class="alert alert-danger">Invalid request.</div>';
}
?>
    <div class="text-center">
        <button type="button" name="new" id="new" onclick="window.location.href='newExpense'" class="btn btn-primary">
            New Expense
        </button>
        <button type="button" name="home" id="new" onclick="window.location.href='index'" class="btn btn-success">Done
        </button>
    </div>

<?php
include 'inc/footer.php';
